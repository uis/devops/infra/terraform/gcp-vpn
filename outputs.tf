# outputs.tf defines outputs for the module.

output "ha_gateway" {
  description = "Name of HA VPN gateway"
  value       = google_compute_ha_vpn_gateway.ha_gateway
}

output "external_gateway" {
  description = "Name of externally managed VPN gateway"
  value       = google_compute_external_vpn_gateway.external_gateway
}

output "cloud_router" {
  description = "Details of cloud router"
  value       = google_compute_router.router
}

output "tunnels" {
  description = "VPN tunnel names"
  value = {
    for name in keys(var.tunnels) :
    name => google_compute_vpn_tunnel.tunnels[name]
  }
}
