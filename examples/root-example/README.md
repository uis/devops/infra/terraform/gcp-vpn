# Simple example
This is a simple example of creating a HA VPN into a GCP project. Specify the project, the self link of network, 
the region and the name to deploy into on the command line. For example, to create a HA VPN in project `host-project`:  

```hcl-terraform
$ terraform init
$ terraform apply -var 'project=host-project' \  
                  -var 'region=europe-west2' \ 
                  -var 'network=https://www.googleapis.com/compute/v1/projects/<PROJECT_ID>/global/networks/default' \
                  -var 'name=peer'
``` 

It is recommended to create common resources (like VPN) in a host (shared VPC) project.  
