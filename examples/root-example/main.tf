module "vpn" {
  source  = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-vpn.git"
  project = var.project
  region  = var.region
  network = var.network
  name    = var.name

  external_gateway = {
    redundancy_type = "SINGLE_IP_INTERNALLY_REDUNDANT"
    interfaces = [{
      id         = 0
      ip_address = "1.1.1.1" # peer gateway IP
    }]
  }
  router         = "cloud-router"
  router_asn     = 64514
  route_priority = 100
  tunnels = {
    t0 = {
      shared_secret              = "random-secret"
      vpn_gateway_interface      = 0
      external_gateway_interface = 0
      ike_version                = 2

      # router-interface
      bgp_session_ip_range = "169.254.1.1/30"

      # router-peer
      bgp_peer_options = null
      bgp_peer = {
        ip_address = "169.254.1.2"
        asn        = 64513
      }
    }
    t1 = {
      description                = ""
      shared_secret              = "random-secret"
      vpn_gateway_interface      = 1
      external_gateway_interface = 0
      ike_version                = 2

      # router-interface
      bgp_session_ip_range = "169.254.2.1/30"

      # router-peer
      bgp_peer_options = null
      bgp_peer = {
        ip_address = "169.254.2.2"
        asn        = 64513
      }
    }
  }
}