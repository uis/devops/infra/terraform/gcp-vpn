variable "project" {
  description = "Project containing VPN setup"
}

variable "region" {
  default = "Region used to create VPN resource"
}

variable "network" {
  description = "Name of the VPC resource that this VPN gateway is accepting traffic for"
}

variable "name" {
  description = "Name of the VPN gateway"
}