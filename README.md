# GCP HA VPN terraform module
This module creates a HA (high availability) cloud VPN that connects to a peer network through IPsec VPN connection.  

The components/resources that this module will create include:  
* A HA VPN gateway
* A peer VPN gateway
* A cloud router
* VPN tunnel(s), and BGP session(s)

This module conforms to the [terraform standard module structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).    

This module configures the VPN in a High Availability (HA) setup 

It is recommended to create common resources (like VPN) in a shared VPC of a host project.  

The HA gateway, external gateway, cloud router and tunnels are available from the project outputs. 

For more information, please refer to:  
* [GCP - creating HA VPN](https://cloud.google.com/vpn/docs/how-to/creating-ha-vpn)
* [GCP - Cloud VPN best practices](https://cloud.google.com/vpn/docs/resources/best-practices)

# Usage Examples
The [examples](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-vpn/-/tree/master/examples) directory contains examples of use. 
