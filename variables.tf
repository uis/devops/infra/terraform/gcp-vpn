# variables.tf defines inputs for the module

variable "project" {
  description = "Project containing VPN setup"
}

variable "region" {
  description = "Region used to create VPN resource"
}

variable "network" {
  description = "Name of the VPC resource that this VPN gateway is accepting traffic for"
}

variable "name" {
  description = "Name of the VPN gateway"
}

variable "external_gateway" {
  description = "Name of the externally managed VPN gateway"
  type = object({
    redundancy_type = string
    interfaces = list(object({
      id         = number
      ip_address = string
    }))
  })
  default = null
}

variable "router" {
  description = "Name of the cloud router"
  default     = "cloud-router"
}

variable "router_asn" {
  description = "BGP ASN of the cloud router"
  type        = number
  default     = 64514
}

variable "route_priority" {
  description = "The priority of routes advertised to BGP peer"
  type        = number
  default     = 100
}

variable "router_advertisement_config" {
  description = "Custom advertisement configurations of the cloud router"
  type = object({
    groups    = list(string)
    ip_ranges = map(string)
    mode      = string
  })
  default = null
}

variable "tunnels" {
  description = "VPN tunnel configurations"
  type = map(object({
    shared_secret              = string
    vpn_gateway_interface      = number
    external_gateway_interface = number
    ike_version                = number

    # router interface
    bgp_session_ip_range = string

    # router peer
    bgp_peer = object({
      ip_address = string
      asn        = number
    })
    bgp_peer_options = object({
      route_priority      = number
      advertise_mode      = string
      advertise_groups    = list(string)
      advertise_ip_ranges = map(string)
    })
  }))
  default = {}
}