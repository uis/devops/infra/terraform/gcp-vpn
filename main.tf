# main.tf contains the top-level resources created by this module.

# A VPN service which creates HA VPN than classic Target VPN gateway
resource "google_compute_ha_vpn_gateway" "ha_gateway" {
  provider = google-beta
  project  = var.project
  region   = var.region
  network  = var.network
  name     = "vpn-gw-gcp-${var.name}"
}

# A VPN gateway managed outside of GCP
resource "google_compute_external_vpn_gateway" "external_gateway" {
  provider        = google-beta
  project         = var.project
  name            = "vpn-gw-${var.name}-gcp"
  redundancy_type = var.external_gateway.redundancy_type
  dynamic "interface" {
    for_each = var.external_gateway.interfaces
    content {
      id         = interface.value.id
      ip_address = interface.value.ip_address
    }
  }
}

# A cloud router service
resource "google_compute_router" "router" {
  project = var.project
  region  = var.region
  name    = var.router
  network = var.network
  bgp {
    asn = var.router_asn
    advertise_mode = (
      var.router_advertisement_config == null ? null : var.router_advertisement_config.mode
    )
    advertised_groups = (
      var.router_advertisement_config == null
      ? null
      : (var.router_advertisement_config.mode == "DEFAULT"
        ? null
      : var.router_advertisement_config.groups)
    )
    dynamic "advertised_ip_ranges" {
      for_each = (
        var.router_advertisement_config == null
        ? {}
        : (var.router_advertisement_config.mode == "DEFAULT"
          ? null
        : var.router_advertisement_config.ip_ranges)
      )
      iterator = range
      content {
        range       = range.key
        description = range.value
      }
    }
  }
}

# A VPN tunnel resource and its configurations
resource "google_compute_vpn_tunnel" "tunnels" {
  for_each                        = var.tunnels
  provider                        = google-beta
  project                         = var.project
  region                          = var.region
  name                            = "tunnel-${var.name}-${each.key}"
  shared_secret                   = each.value.shared_secret
  vpn_gateway                     = google_compute_ha_vpn_gateway.ha_gateway.self_link
  vpn_gateway_interface           = each.value.vpn_gateway_interface
  peer_external_gateway           = google_compute_external_vpn_gateway.external_gateway.self_link
  peer_external_gateway_interface = each.value.external_gateway_interface
  router                          = google_compute_router.router.self_link
  ike_version                     = each.value.ike_version
}

resource "google_compute_router_interface" "router_interface" {
  for_each   = var.tunnels
  project    = var.project
  region     = var.region
  name       = "router-interface-${var.name}-${each.key}"
  router     = google_compute_router.router.name
  ip_range   = each.value.bgp_session_ip_range == "" ? null : each.value.bgp_session_ip_range
  vpn_tunnel = google_compute_vpn_tunnel.tunnels[each.key].name
}

resource "google_compute_router_peer" "router_peer" {
  for_each        = var.tunnels
  project         = var.project
  region          = var.region
  name            = "router-peer-${var.name}-${each.key}"
  router          = google_compute_router.router.name
  interface       = google_compute_router_interface.router_interface[each.key].name
  peer_ip_address = each.value.bgp_peer.ip_address
  peer_asn        = each.value.bgp_peer.asn
  advertised_route_priority = (
    each.value.bgp_peer_options == null
    ? var.route_priority
    : (each.value.bgp_peer_options.route_priority == null
      ? var.route_priority
    : each.value.bgp_peer_options.route_priority)
  )
  advertise_mode = (
    each.value.bgp_peer_options == null
    ? null
    : each.value.bgp_peer_options.advertise_mode
  )
  advertised_groups = (
    each.value.bgp_peer_options == null
    ? null
    : (each.value.bgp_peer_options.advertise_mode == "DEFAULT"
      ? null
    : each.value.bgp_peer_options.advertise_groups)
  )
  dynamic "advertised_ip_ranges" {
    for_each = (
      each.value.bgp_peer_options == null
      ? {}
      : (each.value.bgp_peer_options.advertise_mode == "DEFAULT"
        ? {}
      : each.value.bgp_peer_options.advertise_ip_ranges)
    )
    iterator = range
    content {
      range       = range.key
      description = range.value
    }
  }
}
